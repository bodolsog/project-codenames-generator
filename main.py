#!/usr/bin/env python3

import os
from typing import List

import click

from thrushes import Thrushes

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.group(context_settings=CONTEXT_SETTINGS)
def cli():
    """Project names generator"""
    pass


@cli.command()
def generate():
    """Randomly select generated project name from thrushes db"""
    thrushes = Thrushes()
    selected = thrushes.get_random()
    click.echo(selected.en_project_name)
    return selected.en_project_name


@cli.command()
@click.option(
    "-d",
    "--directory",
    type=click.Path(
        exists=True, file_okay=False, dir_okay=True, writable=True, readable=True
    ),
    required=True,
)
@click.pass_context
def create(ctx: click.Context, directory: str):
    """Create new generated project directory in given path"""

    projects = _get_projects_list(directory)
    project_name = _find_unique_project_name(ctx, projects)
    _create_project_dir(directory, project_name)
    click.echo(f"Successfully created project {project_name}")


def _get_projects_list(directory: str) -> List[str]:
    return [
        name
        for name in os.listdir(directory)
        if os.path.isdir(os.path.join(directory, name))
    ]


def _find_unique_project_name(ctx: click.Context, projects: List[str]):
    while True:
        project_name = ctx.invoke(generate)
        if project_name not in projects:
            return project_name


def _create_project_dir(directory: str, project_name: str):
    os.mkdir(os.path.join(directory, project_name))


if __name__ == "__main__":
    cli()
